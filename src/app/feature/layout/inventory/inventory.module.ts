import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { InventoryComponent } from '../inventory/inventory.component';
// import bulmaAccordion from '/node_modules/bulma-extensions/bulma-calendar/dist/bulma-calendar.min.js';

const routes: Routes = [
  { path: '', component: InventoryComponent },
  { path: '**', component: InventoryComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  declarations: [InventoryComponent]
})
export class InventoryModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {enableProdMode} from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    // if (ENV === 'production') {
    //   enableProdMode();
    // }

    if (this.router.url == '/'){
      this.router.navigate(["/dashboard"]);
    }
  }

  toggleModal() {
    $("#modal").removeClass('is-active');
    $("#modal").removeClass('fadeIn');
    $("#modal").addClass('fadeOut');
  }
}

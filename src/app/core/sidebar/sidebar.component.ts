import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //Toggling accordion in the sidebar
    $('.toggle').click(function(e) {
      e.preventDefault();
  
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
          
          $this.parent().parent().find('.fa-caret-down').addClass('fa-caret-up');
          $this.parent().parent().removeClass('fa-caret-down');
      } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
          
          $this.parent().parent().find('.fa-caret-up').addClass('fa-caret-down');
          $this.parent().parent().removeClass('fa-caret-down');
      }
    });
  }

  openModalInstance(modalId: string) {
    // $(modalId).attr('class','is-active');
    // $("#disp-modal-ter .modal").attr('class', 'is-active')
    console.log();
  }
}

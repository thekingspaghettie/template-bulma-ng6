import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }
  
  count : Number = 1;

  ngOnInit() {
  }
  
  onToggleSidebar() {
    if(this.count == 1) {
      $("#sidebar").width(0);
      $("#main").css('margin-left','0px');
      this.count = 0;
    }
    else {
      $("#sidebar").width(246);
      $("#main").css('margin-left','195px');
      this.count = 1;
    }
  }

}
